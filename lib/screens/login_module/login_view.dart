import '../../constants/app.export.dart';
import 'login_contoller.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(context) {
    return GetBuilder<LoginController>(
      init: LoginController(),
      dispose: (_) => Get.delete<LoginController>(),
      builder: (_) {
        return Scaffold(
          body: mainBody(_),
        );
      },
    );
  }

  mainBody(LoginController _) {
    return SafeArea(
      child: Column(
        children: [
          SizedBox(height: Utils.getSize(130)),
          InkWell(
            onTap: () {
              Utils.showConfirmDialog(
                title: "Are you Sure?",
                description: "Do you want to logout ?",
                negativeTap: () {
                  print("object tape negative");
                },
                positiveTap: () {
                  print("object tape positive");
                },
              );
            },
            child: getHeaderImageView(),
          ),
          BaseRaisedButton(
            buttonText: "Done",
            onPressed: () {
            },
          )
        ],
      ),
    );
  }

  getHeaderImageView() {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: Utils.getSize(0)),
        child: Image.asset(
          Utils.getAssetsImg("login_image"),
          height: Utils.getSize(170),
          width: Utils.getSize(257),
        ),
      ),
    );
  }
}
