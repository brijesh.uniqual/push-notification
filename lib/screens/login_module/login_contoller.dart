
import 'package:get/get.dart';

import '../../helper/push_notification_helper.dart';

class LoginController extends GetxController {
  RxInt counter = 0.obs;

  increment() => counter.value++;
  PushNotificationHelper? pushNotificationHelper;

  reset() {
    counter.value = 0;
    update();
  }

  void onInit(){
    pushNotificationHelper = PushNotificationHelper();
    pushNotificationHelper?.initPush();
    super.onInit();
  }

}
