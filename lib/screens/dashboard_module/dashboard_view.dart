import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:untitled/constants/constant.dart';
import 'package:untitled/helper/push_notification_helper.dart';
import 'package:untitled/screens/dashboard_module/dashboard_controller.dart';

import '../../constants/app.export.dart';
import 'package:timezone/timezone.dart' as tz;

class DashboardView extends StatelessWidget {
  String title;
  String notificationId;
  DashboardView({super.key, required this.title, required this.notificationId});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(
        init: DashboardController(),
        dispose: (_) => Get.delete<DashboardController>(),
        builder: (_) {
          return Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  BaseText(text: "Notification"),
                  10.heightSpacer,
                  BaseText(text: title),
                  BaseText(text: notificationId),
                  10.heightSpacer,
                  BaseRaisedButton(
                    buttonHorizontalPadding: 18.getSize,
                    buttonVerticalPadding: 18.getSize,
                    buttonText: 'Send Notification',
                    onPressed: () async {
                      // var d = await Injector.flutterLocalNotificationsPlugin!.pendingNotificationRequests();
                      //
                      // print("pending notification title=============>${d.first.title}");
                      print("before");
                      tz.TZDateTime now = tz.TZDateTime.now(tz.local);
                      now = now.add(const Duration(seconds: 15));
                      
                      PushNotificationHelper.scheduleLocalNotification(1, "brijesh", "asdfghjkl", now);

                      print("after");
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
