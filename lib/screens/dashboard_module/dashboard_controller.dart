
import 'package:untitled/constants/app.export.dart';

import '../../helper/push_notification_helper.dart';

class DashboardController extends GetxController {

  PushNotificationHelper? pushNotificationHelper;

  @override
  void onInit() {

    pushNotificationHelper = PushNotificationHelper();
    pushNotificationHelper?.initPush();

    super.onInit();
  }

}