class PrefKeys {
  static const String accessToken = "accessToken";
  static const String refreshToken = "refreshToken";
  static const String deviceId = "deviceId";
  static const String providerType = "providerType";
}
